//
// UsersViewController.swift
// Created on 7/26/20
// Created by MIKHAIL ADRIANNE NG

import UIKit
import Foundation

class UsersViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var statusView: UIView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var statusContainer: UIView!
    @IBOutlet var searchTextfield: UITextField!
    
    var userDataSource = [Root](){
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    var searchUsers:[Root] = []
        
    var searchInput: String = ""
    var isSearching:Bool = false
    
    
    let inetReachability = InternetReachability()!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchTextfield.delegate = self
        
        statusView.layer.cornerRadius = 7.5
        statusContainer.layer.cornerRadius = 15
        statusContainer.layer.borderWidth = 1
        
        inetReachability.whenReachable = { _ in
            DispatchQueue.main.async {
                self.statusView.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                self.statusLabel.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                self.statusContainer.layer.borderColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                self.statusLabel.text = "Online"
                print("Internet is OK!")
            }
        }
        inetReachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                self.inetAlert()
                self.statusView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                self.statusLabel.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                self.statusContainer.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                self.statusLabel.text = "Offline"
                print("Internet connection FAILED!")
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: inetReachability)
        
        do {
            try inetReachability.startNotifier()
        } catch {
            print("Could not start notifier")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //declare this inside of viewWillAppear
        
        loadInitial()
        
    }
    
    func loadInitial(){
        let userRequest = UserRequest()
        userRequest.getUsers { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let users):
//                self?.listOfUsers = [Root]
                self?.userDataSource = users
                print(users)
            }
        }
    }
    
    
    @IBAction func searchEditingChanged(_ sender: Any) {
        
        if searchTextfield.text!.count > 0 {
            isSearching = true
            let result = userDataSource.filter({ $0.login.containsIgnoringCase(find: searchTextfield.text ?? "") })
            if result.count > 0 {
                searchUsers = result
                tableView.reloadData()
            } else {
                
            }
        } else {
            isSearching = false
            loadInitial()
        }

        
    }
    
    //MARK:- CHECK FOR INTERNET CONNECTION
    
    @objc func internetChanged(note:Notification) {
        
        let reachability =  note.object as! InternetReachability
        
        //reachability.isReachable is deprecated, right solution --> connection != .none
        if reachability.connection != .none {
            
            //reachability.isReachableViaWiFi is deprecated, right solution --> connection == .wifi
            if reachability.connection == .wifi {
                DispatchQueue.main.async {
                    self.statusView.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                    self.statusLabel.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                    self.statusContainer.layer.borderColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
                    self.statusLabel.text = "Online"
                    //                           self.infoLbl.text = "Internet via WIFI is OK!"
                }
                
            } else {
                DispatchQueue.main.async {
                    self.statusView.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                    self.statusLabel.textColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                    self.statusContainer.layer.borderColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
                    self.statusLabel.text = "Online"
                    //                            self.infoLbl.text = "Internet via Cellular is OK!"
                }
            }
        } else {
            inetAlert()
            //                    self.infoLbl.text = "No Internet connection!"
            self.statusView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            self.statusLabel.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            self.statusContainer.layer.borderColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            self.statusLabel.text = "Offline"
        }
    }
    
    func inetAlert() {
        //Alert Pop-up no internet connection
        let alertController = UIAlertController(title: "", message: "Please, check your internet connection", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        self.view?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
}

extension UsersViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}


extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching == true {
            return searchUsers.count
        } else {
            return userDataSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UsersTableViewCell.identifier, for: indexPath) as! UsersTableViewCell
        cell.selectionStyle = .none
        
        let detail = userDataSource[indexPath.row]

        
        cell.usernameLabel.text = detail.login
        
        
        let imgRequest = DetailRequest(name: detail.login)
        imgRequest.getUserDetail { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let imgOutPut):
                DispatchQueue.main.async {
                    cell.userImage.downloaded(from: "\(imgOutPut.avatar_url)")
                }
            }
        }
        
//        cell.detailLabel.text = detail.avatarURL
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let detail = userDataSource[indexPath.row]
                        
        let storyBoard = UIStoryboard(name: "Users", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        vc.selectedName = detail.login
        show(vc, sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}




extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension String {
            
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
