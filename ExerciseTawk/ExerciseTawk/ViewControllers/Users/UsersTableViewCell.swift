//
// UsersTableViewCell.swift
// Created on 7/26/20
// Created by MIKHAIL ADRIANNE NG

import UIKit

class UsersTableViewCell: UITableViewCell {
    
    static let identifier = "UsersTableViewCell"
            
    @IBOutlet weak var cellContainer: UIView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet var userImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        cellContainer.layer.cornerRadius = 8
        cellContainer.layer.shadowColor = UIColor.black.cgColor
        cellContainer.layer.shadowOffset = CGSize(width: 0, height: 2)
        cellContainer.layer.shadowOpacity = 0.11
        cellContainer.layer.shadowRadius = 10
            
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
