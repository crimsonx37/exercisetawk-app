//
// UserDetailViewController.swift
// Created on 7/26/20

import UIKit

class UserDetailViewController: UIViewController {

    @IBOutlet var followersLbl: UILabel!
    @IBOutlet var followingLbl: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var companyLabel: UILabel!
    @IBOutlet var blogLabel: UILabel!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var detailContainer: UIView!
    @IBOutlet var titlenameLabel: UILabel!
    
    var selectedName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        detailContainer.layer.borderColor = UIColor.black.cgColor
        detailContainer.layer.borderWidth = 1
        
        
        let usrRequest = DetailRequest(name: selectedName)
        usrRequest.getUserDetail { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let userOutput):
                DispatchQueue.main.async {
                    self?.titlenameLabel.text = userOutput.login
                    self?.userImage.downloaded(from: "\(userOutput.avatar_url)")
                    self?.nameLabel.text = "name: \(userOutput.login)"
                    self?.companyLabel.text = "company: \(userOutput.company)"
                    if let Blog = userOutput.blog {
                        self?.blogLabel.text = "blog: \(Blog)"
                    }

                }
            }
        }
        
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
