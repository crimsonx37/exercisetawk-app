//
// UserUrl.swift
// Created on 7/26/20

import Foundation

struct UserUrlResponse:Decodable {
    var response:UserUrls
}

struct UserUrls:Decodable {
    var userUrls:[UserUrlDetail]
}


struct UserUrlDetail:Decodable {
    var login:String
    var id:Int
    var company:String
    var blog:String
    
}
