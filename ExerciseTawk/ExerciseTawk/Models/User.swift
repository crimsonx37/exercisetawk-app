//
// User.swift
// Created on 7/26/20

import Foundation

struct Root:Codable {
    var login: String
    var id: Int
}

struct UserElement: Codable {
    var login: String
    var id: Int
    var avatar_url: String
    var followers_url: String?
    var following_url: String?
    var company: String?
    var blog: String?
}

