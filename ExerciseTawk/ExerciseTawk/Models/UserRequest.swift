//
// UserRequest.swift
// Created on 7/26/20

import Foundation

enum UserError: Error {
    case noDataAvailable
    case canNotProcessData
}


struct UserRequest {
    let resourceURL:URL
    let USER_API_KEY = "https://api.github.com/users?since=0"
    
    init(){
        let resourceString = "\(USER_API_KEY)"
                                
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
                
        self.resourceURL = resourceURL
    }
    
    func getUsers(completion: @escaping(Result<[Root], UserError>) -> Void){
        let dataTask = URLSession.shared.dataTask(with: resourceURL) {data, _, _ in
            guard let jsonData = data else {
                completion(.failure((.noDataAvailable)))
                return
            }
            do {
                let rootResponse = try JSONDecoder().decode([Root].self, from: jsonData)
                let rootDetail = rootResponse
                completion(.success(rootDetail))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
            
}

struct DetailRequest{

    let imageURL:URL
    let IMAGE_API_KEY = "https://api.github.com/users/"

    init(name: String){
        let imageString = "\(IMAGE_API_KEY)\(name)"

        guard let imageURL = URL(string: imageString) else {fatalError()}

        self.imageURL = imageURL
    }

    func getUserDetail(completion: @escaping(Result<UserElement, UserError>) -> Void){
           let dataTask = URLSession.shared.dataTask(with: imageURL) {data, _, _ in
               guard let jsonData = data else {
                   completion(.failure((.noDataAvailable)))
                   return
               }
               do {
                   let userResponse = try JSONDecoder().decode(UserElement.self, from: jsonData)
                   let userDetail = userResponse
                   completion(.success(userDetail))
               } catch {
                   completion(.failure(.canNotProcessData))
               }
           }
           dataTask.resume()
       }
}
